var express = require('express');
var router = express.Router();
var db = require('../config/db');
// var bcrypt = require('bcrypt');



/* GET home page. */
router.get('/', function (req, res, next) {
  res.redirect('/login');
});

//Register System
// Register
router.get('/register', function (req, res, next) {
  res.render('register', { message: null });
});

// check user 
router.post('/register', function (req, res, next) {
  // var password = req.body.password;
  // //生成salt的迭代次数
  // const saltRounds = 10;
  // //随机生成salt
  // const salt = bcrypt.genSaltSync(saltRounds);
  // //获取hash值
  // var hash = bcrypt.hashSync(password, salt);
  //  //把hash值赋值给password变量
  // password = hash;

  var queryString = "select * from users where username='" + req.body.username + "'";
  db.query(queryString, function (err, rows) {
    if (err) {
      res.send(err);
    } else if (rows.length != 0) {
      res.render("register.ejs", { message: 'User is exist!!' });
    } else if (req.body.password != req.body.repassword) {
      res.render("register.ejs", { message: 'Password must same!!' });
    } else if (req.body.username.length == 0) {
      res.render("register.ejs", { message: 'Username is required!!' });
    } else if (req.body.password.length == 0) {
      res.render("register.ejs", { message: 'Password is required!!' });
    } else {
      db.query("insert into users(username, password) values('" + req.body.username + "', '" + req.body.password + "')");
      res.render("register.ejs", { message: 'Success register user' });
    }
  })
});

//Login System
//check login state
router.get('/login', function (req, res, next) {
  if (req.session.userId) {
    res.redirect('/event');
  } else {
    next();
  }
});

//login
router.get('/login', function (req, res, next) {
  res.render('login', { message: null });
});

router.post('/login', function (req, res, next) {
  // var password = req.body.password;
  var queryString = "select * from users where username='" + req.body.username + "'";
  db.query(queryString, function (err, rows) {
    // var pwdMatchFlag =bcrypt.compareSync(password, rows[0].password);
    if (err) {
      res.send(err);
    } else {
      if (rows.length == 0) {
        res.render("login.ejs", { message: 'User is not found!!' });
      } else if (req.body.password == rows[0].password) {
        req.session.userId = rows[0].id;
        req.session.username = rows[0].username;
        return res.redirect('/event');
      } else {
        res.render("login.ejs", { message: 'Password is wrong!!' });
      }
    }
  })
});

//logout
router.get('/logout', function (req, res) {
  req.session.userId = null;
  return res.redirect('/login');
})

//get users info
router.get('/profile', function (req, res, next) {
  var userId = req.session.userId;
  db.query("select * from users where id='" + userId + "'", function (err, user) {
    res.render('profile.ejs', { user: user[0] });
  })
})

//User - edit user password
router.get('/edit', function (req, res, next) {
  var userId = req.session.userId;
  db.query('SELECT * FROM users WHERE id = ?', userId, function (err, rs) {
    res.render('form', { user: rs[0] });
  });
})

router.put('/editUser', function (req, res, next) {
  var userId = req.session.userId;
  var param = [
    // req.body.username,     //data for update
    req.body.password,
    userId //condition for update
  ]
  console.log(param);
  db.query('UPDATE users SET password = ?  WHERE id = ?', param, function (err, rs, fields) {
    if (err) {
      res.send(err);
    } else {
      res.redirect('/login');
    }
  });
});


//Event Function
//Show All event 
router.get('/event', function (req, res, next) {
  db.query('SELECT event_id , username, event_name, data, place, description FROM users, event WHERE users.id = event.id ORDER BY data DESC', function (err, rs) {
    res.render('event.ejs', { event: rs});
  });
});

//Event - User create new event
router.get('/insertEventForm', function (req, res, next) {
  var userId = req.session.userId;
  db.query("select * from users where id='" + userId + "'", function (err, rs) {
    res.render('insertEventForm', { user: rs[0] });
  });
});

router.post('/insertEventForm', function (req, res, next) {
  db.query('INSERT INTO event SET ?', req.body, function (err, rs) {
    res.redirect('/event');
  });
});

//Event - show event who created
router.get('/myEvent', function (req, res, next) {
  var userId = req.session.userId;
  db.query('SELECT event_id, username, event_name, data, place, description FROM users as u join event as e on u.id = e.id WHERE e.id =' + userId + ' ORDER BY data DESC', function (err, rs) {
    res.render('myEvent.ejs', { event: rs });
  });
});

//Event - edit event
router.get('/editEvent', function (req, res, next) {
  db.query('SELECT * FROM event WHERE event_id = ?', req.query.event_id, function (err, rs) {
    res.render('editEventForm', { event: rs[0] });
  });
});

router.put('/editEventDesc', function (req, res, next) {
  var param = [
    req.body.id,
    req.body.event_name,
    req.body.data,
    req.body.place,
    req.body.description,
    req.body.event_id //condition for update
  ]
  console.log(param);
  db.query('UPDATE event SET id = ? , event_name = ?  , data = ? , place = ? , description = ? WHERE event_id = ?', param, function (err, rs, fields) {
    if (err) {
      res.send(err);
    } else {
      res.redirect('/login');
    }
  });
});

//Join function
// Join - User join event
router.get('/insertjoinEvent', function (req, res, next) {
  db.query('SELECT * FROM users as u join event as e on u.id = e.id  WHERE e.event_id = ' + req.query.event_id + '', function (err, rs) {
    req.session.event_id = rs[0].event_id;
    res.render('insertjoinEvent', { joinEvent: rs[0] });
  });
});

router.post('/insertjoinEvent', function (req, res, next) {
  var userId = req.session.userId;
  var eventId = req.session.event_id;
  var param = [
    eventId,
    userId
  ]
  console.log(param);
  var queryString = "select * from joinEvent where id= '" + userId + "'";
  db.query(queryString, function (err, rows) {
    if (err) {
      res.send(err);
    } else if (rows.length != 0) {
      res.redirect("/event");
    } else {
      db.query('INSERT INTO joinEvent SET event_id = ?, id = ?', param, function (err, rs) {
        res.redirect('/joinEvent');
      });
    }
  });
});

//Join - show event which is you have join 
router.get('/joinEvent', function (req, res, next) {
  var userId = req.session.userId;
  db.query('SELECT * FROM joinEvent as j join event as e on e.event_id = j.event_id join users as u on u.id = j.id WHERE j.id =' + userId + ' ORDER BY data DESC', function (err, rs) {
    res.render('joinEvent.ejs', { event: rs });
  });
});

//join - show number of join
router.get('/joinDetail', function (req, res, next) {
  var userId = req.session.userId;
  db.query('SELECT COUNT(*) as eventCount FROM joinEvent WHERE event_id =' + req.query.event_id + ' GROUP BY event_id ', function (err, rs) {
    res.render('joinDetail.ejs', { event: rs });
  });
});

//join - cancel join event
router.get('/deleteJoinEvent', function (req, res, next) {
  db.query('SELECT * FROM joinEvent WHERE event_id = ?', req.query.event_id, function (err, rs) {
    res.render('deleteJoinEventConfirm', { event: rs[0] });
  });
});

router.delete('/deleteJoinEventDesc', function (req, res, next) {
  var param = [
    req.body.event_id //condition for update
  ]
  console.log(param);
  db.query('DELETE FROM joinEvent WHERE event_id = ?', param, function (err, rs, fields) {
    if (err) {
      res.send(err);
    } else {
      return res.redirect('/joinEvent');
    }
  })
});

//Comment function
//Comment - User create comment
router.get('/insertEventComment', function (req, res, next) {
  db.query('SELECT * FROM users as u join event as e on u.id = e.id  WHERE e.event_id = ' + req.query.event_id + '', function (err, rs) {
    req.session.event_id = rs[0].event_id;
    res.render('insertEventComment', { comment: rs[0] });
  });
});

router.post('/insertEventComment', function (req, res, next) {
  var userId = req.session.userId;
  var eventId = req.session.event_id;
  var param = [
    eventId,
    userId,
    req.body.comment,
    req.body.date
  ]
  console.log(param);
  db.query('INSERT INTO comment SET event_id = ?, id = ? , comment = ? , date = ? ', param, function (err, rs) {
    res.redirect('/joinEvent');
  });
});

//Comment - User can view comment in all event
router.get('/viewEventComment', function (req, res, next) {
  db.query('SELECT * FROM event as e join comment as c on e.event_id = c.event_id join users as u on u.id =c.id WHERE e.event_id =  ' + req.query.event_id + ' ORDER BY date DESC', function (err, rs) {
    res.render('viewEventComment.ejs', { event: rs });
  });
});

//comment - show comment who is you to insert
router.get('/myComment', function (req, res, next) {
  var userId = req.session.userId;
  db.query('SELECT * FROM comment as c join event as e on e.event_id = c.event_id join users as u on u.id = c.id WHERE c.id = ? ', userId, function (err, rs) {
    res.render('myComment.ejs', { event: rs });
  });
});

//comment - edit comment
router.get('/editEventComment', function (req, res, next) {
  db.query('SELECT * FROM comment WHERE comment_id = ?', req.query.comment_id, function (err, rs) {
    res.render('editEventComment', { event: rs[0] });
  });
});

router.put('/editEventCommentDesc', function (req, res, next) {
  var param = [
    req.body.event_id,
    req.body.id,
    req.body.comment,
    req.body.date,
    req.body.comment_id //condition for update
  ]
  console.log(param);
  db.query('UPDATE comment SET event_id = ? , id = ?  , comment = ? , date = ?  WHERE comment_id = ?', param, function (err, rs, fields) {
    if (err) {
      res.send(err);
    } else {
      res.redirect('/myComment');
    }
  });
});

// Join - delete comment
router.get('/deleteEventComment', function (req, res, next) {
  db.query('SELECT * FROM comment WHERE comment_id = ?', req.query.comment_id, function (err, rs) {
    res.render('deleteEventCommmetConfirm', { event: rs[0] });
  });
})

router.delete('/deleteEventCommentDesc', function (req, res, next) {
  var param = [
    req.body.comment_id //condition for update
  ]
  console.log(param);
  db.query('DELETE FROM comment WHERE comment_id = ?', param, function (err, rs, fields) {
    if (err) {
      res.send(err);
    } else {
      return res.redirect('/myComment');
    }
  })
})


// router.get('/deleteUser', function (req, res, next) {
//   var userId = req.session.userId;
//   db.query('SELECT * FROM users WHERE id = ?', userId, function (err, rs) {
//     res.render('deleteUser', { user: rs[0] });
//   });
// })

// router.delete('/delete', function (req, res, next) {
//   var eventId = req.session.event_id;
//   var param = [
//     req.body.id
//   ]
//   console.log(param);

//   db.query('DELETE FROM comment WHERE id = ?', param, function (err, rs, fields) {
//   })

//   db.query('DELETE FROM event WHERE id = ?', param, function (err, rs, fields) {
//   })
//   req.session.destroy();
//   db.query('DELETE FROM users WHERE id = ?', param, function (err, rs, fields) {
//     if (err) {
//       res.send(err);
//     } else {
//       return res.redirect('/login');
//     }
//   })
// })

//Event - delete event
// router.get('/deleteEvent', function (req, res, next) {
//   db.query('SELECT * FROM event WHERE event_id = ?', req.query.event_id, function (err, rs) {
//     res.render('deleteEventConfirm', { event: rs[0]});
//   });
// })

// router.delete('/deleteEventDesc', function (req, res, next) {
//   var param = [
//     req.body.event_id //condition for update
//   ]
//   console.log(param);
//   db.query('DELETE FROM event WHERE event_id = ?', param , function (err, rs, fields) {
//     if (err) {
//       res.send(err);
//     } else {
//       return res.redirect('/myEvent');
//     }
//   })
// })

module.exports = router;
