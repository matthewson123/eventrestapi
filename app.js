const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const flash = require('connect-flash');
const session = require('express-session');
const bodyParser = require('body-parser');
const methodOverride = require('method-override');
var partials = require('express-partials')

const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views')); 
app.set('view engine', 'ejs');
app.use(partials());

//session
app.use(session({
  cookie: { maxAge: 1000 * 60 * 30 },
  secret: 'secret',
  resave: false,
  saveUninitialized: true
}));

app.use(methodOverride('_method', { methods: ["POST" , "GET"] }));

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(logger('dev'));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, '/views')));
app.use(express.static(path.join(__dirname, '/vendor')));
app.use(express.static(path.join(__dirname, '/css')));
app.use(express.static(path.join(__dirname, '/js')));
// app.use(express.static(__dirname + '/views'));
app.use(flash());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use('/', indexRouter);
app.use('/users', usersRouter);


// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

//connect-flash 
app.use(function (req, res, next) {
  console.log("app.usr local");
  res.locals.user = req.session.user;
  var err = req.flash('error');
  res.locals.error = err.length ? err : null;
  var success = req.flash('success');
  res.locals.success = success.length ? success : null;
  next();
});



module.exports = app;
