var save_btn = $("#update_user");
$(document).ready(function () {
    $(save_btn).on('click', function () {
        // var user = document.getElementById("username").value;
        var pass = document.getElementById("password").value;
        var text = { password: pass };

        $.ajax({
            url: '/editUser',
            type: 'PUT',
            data: JSON.stringify(text),
            contentType: "application/json",
            processData: false
        });
    })
});

var delete_btn = $("#delete_user");
$(document).ready(function () {
    $(delete_btn).on('click', function () {
        var userId = document.getElementById("id").value;
        var text = { id: userId };
        $.ajax({
            url: '/delete',
            type: 'DELETE',
            data: JSON.stringify(text),
            contentType: "application/json",
            processData: false
        });
        window.location.replace('/login');
    })
});