var save_btn = $("#update_comment");
$(document).ready(function () {
    $(save_btn).on('click', function () {
        var u_id = document.getElementById("id").value;
        var e_id = document.getElementById("event_id").value;
        var c = document.getElementById("comment").value;
        var d = document.getElementById("date").value;
        var c_id = document.getElementById("comment_id").value;
        var text = { id: u_id, event_id:e_id, comment:c, date: d, comment_id: c_id };
        $.ajax({
            url: '/editEventCommentDesc',
            type: 'PUT',
            data: JSON.stringify(text),
            contentType: "application/json",
            processData: false
        });
    })
});

var delete_btn = $("#delete_comment");
$(document).ready(function () {
    $(delete_btn).on('click', function () {
        var c_id = document.getElementById("comment_id").value;
        var text = { comment_id: c_id };
        $.ajax({
            url: '/deleteEventCommentDesc',
            type: 'DELETE',
            data: JSON.stringify(text),
            contentType: "application/json",
            processData: false
        });
        window.location.replace('/myComment');
    })
});