var delete_btn = $("#delete_joinEvent");
$(document).ready(function () {
    $(delete_btn).on('click', function () {
        var e_id = document.getElementById("event_id").value;
        var text = { event_id: e_id };
        $.ajax({
            url: '/deleteJoinEventDesc',
            type: 'DELETE',
            data: JSON.stringify(text),
            contentType: "application/json",
            processData: false
        });
        window.location.replace('/joinEvent');
    })
});