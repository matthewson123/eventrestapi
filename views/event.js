var save_btn = $("#update_event");
$(document).ready(function () {
    $(save_btn).on('click', function () {
        var userId = document.getElementById("id").value;
        var eventName = document.getElementById("event_name").value;
        var d = document.getElementById("data").value;
        var p = document.getElementById("place").value;
        var desc = document.getElementById("description").value;
        var e_id = document.getElementById("event_id").value;
        var text = { id: userId, event_name:eventName , data:d , place:p , description:desc , event_id:e_id };

        $.ajax({
            url: '/editEventDesc',
            type: 'PUT',
            data: JSON.stringify(text),
            contentType: "application/json",
            processData: false
        });
    })
});

var delete_btn = $("#delete_event");
$(document).ready(function () {
    $(delete_btn).on('click', function () {
        var e_id = document.getElementById("event_id").value;
        var text = { event_id:e_id };
        $.ajax({
            url: '/deleteEventDesc',
            type: 'DELETE',
            data: JSON.stringify(text),
            contentType: "application/json",
            processData: false
        });
        window.location.replace('/myEvent');
    })
});